async function getData() {
  const res = await fetch("https://jsonplaceholder.typicode.com/users/1")

  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  // Recommendation: handle errors
  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data")
  }

  return res.json()
}

async function getData2() {
  const res = await fetch("https://jsonplaceholder.typicode.com/users/2")

  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.

  // Recommendation: handle errors
  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error("Failed to fetch data")
  }

  return res.json()
}

/*
This above code is taken directly from the NextJS docs 
and the output in being done below , the code for that has been taken
from the main vid. Note you also remember writing similar code when 
doing the Dave Course
*/

export default async function Page() {
  const data = await getData()
  const data2 = await getData2()

  return (
    <main>
      <p> This Data 1 </p>
      {JSON.stringify(data)}
      <p>This Data 2 </p>
      {JSON.stringify(data2)}
    </main>
  )
}
